package com.arczi69.keyvault.hsm;

import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.spring.AzureKeyVaultCredential;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"test"})
public class SPCredentialProvider implements VaultCredentialProvider{
    @Override
    public KeyVaultClient credentials() {
        return new KeyVaultClient(new AzureKeyVaultCredential("b64529af-1cda-4602-a873-ab5107ade19e", "c1LqTF@h4RpE:PmSGlc9aq./asiVnDT6", 10));
    }
}
