package com.arczi69.keyvault.hsm;

import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.spring.AzureKeyVaultMSICredential;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("!test")
public class MSICredentialProvider implements VaultCredentialProvider{
    @Override
    public KeyVaultClient credentials() {
        return new KeyVaultClient(new AzureKeyVaultMSICredential(AzureEnvironment.AZURE));
    }
}
