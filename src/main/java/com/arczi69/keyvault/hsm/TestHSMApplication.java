package com.arczi69.keyvault.hsm;

import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.models.KeyOperationResult;
import com.microsoft.azure.keyvault.spring.AzureKeyVaultCredential;
import com.microsoft.azure.keyvault.webkey.JsonWebKeySignatureAlgorithm;
import org.jose4j.base64url.Base64Url;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwx.CompactSerializer;
import org.jose4j.lang.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.Assert;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class TestHSMApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TestHSMApplication.class, args);
    }

    @Value("${test}")
    private String connectionString;

    @Autowired
    VaultCredentialProvider provider;

    @Override
    public void run(String... args) throws Exception {
        String jwt = createSampleJWT();
        System.out.println(jwt);
    }

    private String createSampleJWT () throws NoSuchAlgorithmException {

        JwtClaims claims = new JwtClaims();
        claims.setIssuer("Artur_homeless");
        claims.setAudience("Artur_basement");
        claims.setExpirationTimeMinutesInTheFuture(60);
        claims.setNotBeforeMinutesInThePast(3);
        claims.setSubject("");

        JsonWebSignature jws = new JsonWebSignature();

        jws.setPayload(claims.toJson());

        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        String signingInput = CompactSerializer.serialize(jws.getHeaders().getEncodedHeader(), jws.getEncodedPayload());
        byte[] bytesToSign = StringUtil.getBytesAscii(signingInput);

        byte[] signature = signWithCloudHsmUsingRsaSha256(bytesToSign, JsonWebKeySignatureAlgorithm.RS256);

        String encodedSignature = Base64Url.encode(signature);
        String jwt = CompactSerializer.serialize(signingInput, encodedSignature);

        return jwt;
    }

    private byte [] signWithCloudHsmUsingRsaSha256 (byte [] contentToSign, JsonWebKeySignatureAlgorithm algorithm) throws NoSuchAlgorithmException {
        //String key = "https://arczi69.vault.azure.net/keys/artur-selfsigned/d0954be994434920b37b568c8693ed3d";
        String key = "https://arczi69.vault.azure.net/keys/artur-selfsigned"; //key without version
        KeyVaultClient kvClient = provider.credentials();

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(contentToSign);
        byte[] digest = md.digest();

        KeyOperationResult kvr = kvClient.sign(key, algorithm, digest);
        Assert.isTrue(kvClient.verify(key, algorithm, digest, kvr.result()).value(), "wrong signature");

        return kvr.result();
    }

}
