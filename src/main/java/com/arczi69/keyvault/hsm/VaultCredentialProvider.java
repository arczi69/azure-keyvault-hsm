package com.arczi69.keyvault.hsm;

import com.microsoft.azure.keyvault.KeyVaultClient;

public interface VaultCredentialProvider {
    KeyVaultClient credentials();
}
