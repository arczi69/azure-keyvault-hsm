# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/maven-plugin/)
* [Azure Key Vault](https://github.com/Microsoft/azure-spring-boot/tree/master/azure-spring-boot-starters/azure-keyvault-secrets-spring-boot-starter)

### Guides
The following guides illustrate how to use some features concretely:

* [Using Key Vault](https://github.com/Microsoft/azure-spring-boot/tree/master/azure-spring-boot-samples/azure-keyvault-secrets-spring-boot-sample)

###Onboarding
VM's have to be allowed to use Sign API. please assign required permissions.
Sample Azure PowerShell script

Set-AzureRMKeyVaultAccessPolicy -vaultname <vault name>> -objectid <objectid - taken from VM identity page> -PermissionsToKeys Sign, Verify